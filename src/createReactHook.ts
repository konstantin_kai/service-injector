import { useEffect, useMemo } from 'react';
import { assert } from './assert';
import { ServiceInjector, ServicesMetadataArgType, WithInjectedServices } from './ServiceInjector';

export const createReactHook = (serviceInjector: ServiceInjector) =>
  <T extends ServicesMetadataArgType>(injectable: T): WithInjectedServices<T> => {
    useEffect(
      () => {
        assert(
          injectable !== null
          && typeof injectable === 'object'
          && Object.keys(injectable).length > 0,
          'injectable should be a non empty object',
        );
      }
    );

    return useMemo(
      () => serviceInjector.getObjects(injectable),
      [injectable],
    );
  };