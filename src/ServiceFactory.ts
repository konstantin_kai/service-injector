import { assert } from './assert';
import { ValueKey } from './ValueKey';

export type Constructor = new (...args: any[]) => any;

export type ObjectCreator<T> = () => T;

export interface ServiceFactoryArgs<T> {
  id: ValueKey;
  type: ServiceFactoryType;
  creator?: ObjectCreator<T>;
  instance?: T;
}

export enum ServiceFactoryType {
  Singleton,
  LazySingleton,
}

/**
 * Main point for instantiating registered objects
 *
 * @typeParam T Type of service object
 */
export class ServiceFactory<T> {
  constructor(args: ServiceFactoryArgs<T>) {
    this.id = args.id;
    this.type = args.type;
    this._creator = args.creator ?? null;
    this._instance = args.instance ?? null;

    assert(
      this.type === ServiceFactoryType.Singleton ? this._instance !== null : true,
      '"instance" should be passed for "ServiceFactoryType.Singleton" type',
    );

    assert(
      this.type === ServiceFactoryType.LazySingleton ? this._creator !== null : true,
      '"creator" should be passed for "ServiceFactoryType.LazySingleton" type',
    );
  }

  static singleton<T>(
    instance: T,
    id: ValueKey,
  ): ServiceFactory<T> {
    return new ServiceFactory({
      id,
      instance,
      type: ServiceFactoryType.Singleton,
    });
  }

  static lazySingleton<T>(
    creator: ObjectCreator<T>,
    id: ValueKey,
  ): ServiceFactory<T> {
    return new ServiceFactory({
      id,
      creator,
      type: ServiceFactoryType.LazySingleton,
    });
  }

  /**
   * Uniq factory id
   */
  readonly id: ValueKey;

  /**
   * Factory type
   */
  readonly type: ServiceFactoryType;

  private _creator: ObjectCreator<T> | null;
  private _instance: T | null;

  getObject(): T {
    switch (this.type) {
      case ServiceFactoryType.Singleton:
        return this._instance!;
      case ServiceFactoryType.LazySingleton:
        if (this._instance === null) {
          this._instance = this._creator!();
        }

        return this._instance!;
      default:
        throw new Error('Cannot create an object');
    }
  }
}
