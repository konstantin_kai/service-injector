import { ValueKey } from '../ValueKey';

describe('ValueKey', () => {
  it('test #1', () => {
    const key1 = new ValueKey('1');
    const key2 = new ValueKey('2');

    expect(key1).not.toEqual(key2);
    expect(key1).toEqual(key1);
    expect(key1).toEqual(new ValueKey('1'));
    expect(key2).not.toEqual(new ValueKey('1'));
    expect(key2).toEqual(new ValueKey('2'));
  });
});
