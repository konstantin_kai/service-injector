import { ServiceInjector } from '../ServiceInjector';
import { ServiceMeta } from '../ServiceMeta';

describe('ServiceInjector', () => {
  it('test #1', () => {
    const injector = new ServiceInjector();

    expect(() => injector.getObject(new ServiceMeta('1')))
      .toThrow('Object with id "1" is not registered in serviceInjector');

    injector.registerSingleton(new ServiceMeta('1'), 'str1');

    expect(() => injector.registerSingleton(new ServiceMeta('1'), 'str1'))
      .toThrow('Object with id "1" already registered in serviceInjector');

    expect(injector.getObject(new ServiceMeta('1'))).toEqual('str1')

    injector.registerLazySingleton(new ServiceMeta('2'), () => 'str2');

    expect(injector.getObject(new ServiceMeta('2'))).toEqual('str2');

    expect(() => injector.registerLazySingleton(new ServiceMeta('2'), () => 'str2'))
      .toThrow('Object with id "2" already registered in serviceInjector');
  });
});
