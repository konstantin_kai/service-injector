import { ServiceFactory, ServiceFactoryType } from '../ServiceFactory';
import { ValueKey } from '../ValueKey';

describe('ServiceFactory', () => {
  it('test #1', () => {
    expect(() => new ServiceFactory({
      id: new ValueKey('1'),
      type: ServiceFactoryType.Singleton,
    })).toThrow('Cannot use "ServiceFactory" without "creator" or "instance"');
  });

  it('test #2', () => {
    const creator = jest.fn();
    const factory = new ServiceFactory({
      id: new ValueKey('1'),
      type: ServiceFactoryType.LazySingleton,
      creator: () => {
        creator();
        return 'str1'
      },
    });

    expect(creator).not.toBeCalled();
    expect(factory.getObject()).toEqual('str1');
    expect(creator).toBeCalledTimes(1);
    expect(factory.getObject()).toEqual('str1');
    expect(creator).toBeCalledTimes(1);
  });
});