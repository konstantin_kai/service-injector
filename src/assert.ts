/**
 * An error that throws by the runtime system when an [[assert]] statement fails.
 */
export class AssertionError extends Error {
  name: string = 'AssertionError';

  constructor(msg?: string) {
    super(msg ?? '');
    Object.setPrototypeOf(this, AssertionError.prototype);
  }
}

type Condition = boolean | (() => boolean);

/**
 * Checks input condition and throws runtime exception [[AssertionError]]
 *
 * **Note:** works only in developer mode
 */
export function assert(condition: Condition, message?: string): asserts condition {
  if (process.env.NODE_ENV === 'production') return;

  if (typeof condition === 'function') {
    condition = condition();
  }

  if (!condition) {
    throw new AssertionError(message);
  }
}
