export class ValueKey {
  private static _cache = new Map<string, ValueKey>();

  constructor(id: string) {
    if (ValueKey._cache.has(id)) {
      return ValueKey._cache.get(id) as ValueKey;
    }

    this.id = id;
    ValueKey._cache.set(id, this);
  }

  readonly id!: string;

  toString(): string {
    return this.id;
  }
}
