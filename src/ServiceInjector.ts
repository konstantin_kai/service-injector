import { assert } from './assert';
import { ServiceFactory, ObjectCreator } from './ServiceFactory';
import { ServiceMeta } from './ServiceMeta';
import { ValueKey } from './ValueKey';

export type ServicesMetadataArgType = {
  [key: string]: ServiceMeta<any>;
}

export type WithInjectedServices<T extends ServicesMetadataArgType> = {
  [P in keyof T]: T[P] extends ServiceMeta<any> ? T[P]['type'] : never
};

export class ServiceInjector {
  private _factories = new WeakMap<ValueKey, ServiceFactory<unknown>>();

  getObject<T>(meta: ServiceMeta<T>): T {
    assert(this._factories.has(meta.id), `Object with id "${meta.id}" is not registered in serviceInjector`);

    return this._factories.get(meta.id)!.getObject() as T;
  }

  getObjects<T extends ServicesMetadataArgType>(metadata: T): WithInjectedServices<T> {
    return Object.entries(metadata).reduce((obj, [key, meta]) => {
      obj[key as keyof T] = this.getObject(meta);

      return obj;
    }, {} as WithInjectedServices<T>);
  }

  registerSingleton<T>(meta: ServiceMeta<T>, object: T): void {
    assert(!this._factories.has(meta.id), `Object with id "${meta.id}" already registered in serviceInjector`);

    const factory = ServiceFactory.singleton(object, meta.id);
    this._factories.set(factory.id, factory);
  }

  registerLazySingleton<T>(meta: ServiceMeta<T>, creator: ObjectCreator<T>): void {
    assert(!this._factories.has(meta.id), `Object with id "${meta.id}" already registered in serviceInjector`);

    const factory = ServiceFactory.lazySingleton(creator, meta.id);
    this._factories.set(factory.id, factory);
  }
}
