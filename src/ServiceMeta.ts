import { ValueKey } from './ValueKey';

/**
 * Meta class for holding service type and single key in {@link ServiceInjector}
 *
 * @typeParam T Type of a service object
 */
export class ServiceMeta<T> {
  constructor(id: string) {
    this.id = new ValueKey(id);
  }

  /**
   * Service meta id
   */
  readonly id: ValueKey;

  /**
   * Always equals `undefined`, **only** for type referring
   */
  readonly type!: T;
}
